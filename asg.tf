provider "aws" {
  region      = var.region
}
resource "aws_launch_configuration" "ES_CLUSTER_LC" {
  name_prefix            = "ES_CLUSTER_LC"
  image_id               = var.image_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  iam_instance_profile   = var.iam_instance_profile
  security_groups        = var.security_groups
#  user_data              = "${file("ansible.sh")}"
   lifecycle {
    create_before_destroy = true
  }

}
resource "aws_autoscaling_group" "ES_CLUSTER_ASG" {
  name                      = "ES_CLUSTER_ASG"
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity
  launch_configuration      = aws_launch_configuration.ES_CLUSTER_LC.name
  vpc_zone_identifier       = [var.vpc_zone_identifier]
  tag {
      key                 = "Name"
      value               = "AutoScaled"
      propagate_at_launch = true
      }
  tag {
  key                 = "Initialized"
  value               = "false"
  propagate_at_launch = true

     }
#  lifecycle {
#    create_before_destroy = true
#  }
}
resource "aws_autoscaling_policy" "scaling_up_policy" {
  name                   = "scaling_up_policy"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = 1
  autoscaling_group_name = aws_autoscaling_group.ES_CLUSTER_ASG.name
}
resource "aws_autoscaling_policy" "scaling_down_policy" {
  name                   = "scaling_down_policy"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = -1
  autoscaling_group_name = aws_autoscaling_group.ES_CLUSTER_ASG.name
}

