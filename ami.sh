ARTIFACT_API=`packer build -machine-readable ansible.json  |awk -F, '$0 ~/artifact,0,id/ {print $6}'`
AMI_ID_API=`echo $ARTIFACT_API | cut -d ':' -f2`
echo 'variable "image_id" { default = "'${AMI_ID_API}'" }' >> variables.tf
