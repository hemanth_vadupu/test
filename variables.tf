variable "region" {
  description = "The EC2 image ID to launch"
  type        = string
  default     = "us-east-1"
}
variable "instance_type" {
  description = "The size of instance to launch"
  type        = string
  default     = "t2.micro"
}

variable "iam_instance_profile" {
  description = "The IAM instance profile to associate with launched instances"
  type        = string
  default     = "ec2-s3"
}

variable "key_name" {
  description = "The key name that should be used for the instance"
  type        = string
  default     = "mynewkey"
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the launch configuration"
  type        = list(string)
  default     = ["sg-083c27d6c6d275283"]
}

variable "max_size" {
  type		= number
  default	= 5
}

variable "min_size" {
  type		= number
  default	= 0
}

variable "desired_capacity" {
  type          = number
  default       = 0
}

variable "vpc_zone_identifier"{
  type		= string
  default	= "subnet-00000cf10df0c90eb"
}


